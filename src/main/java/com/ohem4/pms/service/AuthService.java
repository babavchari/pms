package com.ohem4.pms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ohem4.pms.dao.AuthDAO;
import com.ohem4.pms.dto.LoginDTO;
import com.ohem4.pms.entity.Register;

@Service
public class AuthService {
	
	@Autowired
	private AuthDAO authDAO;
	
	public void saveRegisterDetails(Register register) {
		authDAO.saveRegisterDetails(register);
	}
	
	public Register getRegisterDataByEmailAndPwd(LoginDTO loginDTO) {
		return authDAO.getRegisterDataByEmailAndPwd(loginDTO);
	}
}
